<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ResourceAttributeVersionBackup
 *
 * @ORM\Table(name="resource_attribute_version_backup", indexes={@ORM\Index(name="resource_attribute_version_FI_1", columns={"resource_version_id"})})
 * @ORM\Entity
 */
class ResourceAttributeVersionBackup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="resource_version_id", type="integer", nullable=false)
     */
    private $resourceVersionId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="attribute_name", type="string", length=255, nullable=false)
     */
    private $attributeName = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="attribute_value", type="text", length=65535, nullable=true)
     */
    private $attributeValue;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getResourceVersionId(): ?int
    {
        return $this->resourceVersionId;
    }

    public function setResourceVersionId(int $resourceVersionId): self
    {
        $this->resourceVersionId = $resourceVersionId;

        return $this;
    }

    public function getAttributeName(): ?string
    {
        return $this->attributeName;
    }

    public function setAttributeName(string $attributeName): self
    {
        $this->attributeName = $attributeName;

        return $this;
    }

    public function getAttributeValue(): ?string
    {
        return $this->attributeValue;
    }

    public function setAttributeValue(?string $attributeValue): self
    {
        $this->attributeValue = $attributeValue;

        return $this;
    }


}
