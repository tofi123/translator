<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TrTranslationUnitContext
 *
 * @ORM\Table(name="tr_translation_unit_context", uniqueConstraints={@ORM\UniqueConstraint(name="context_list", columns={"context_list"})})
 * @ORM\Entity
 */
class TrTranslationUnitContext
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="context_list", type="string", length=255, nullable=false)
     */
    private $contextList;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContextList(): ?string
    {
        return $this->contextList;
    }

    public function setContextList(string $contextList): self
    {
        $this->contextList = $contextList;

        return $this;
    }

    public function __toString()
    {
        return (string) $this->getId();
    }
}
