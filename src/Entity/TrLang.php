<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TrLang
 *
 * @ORM\Table(name="tr_lang", uniqueConstraints={@ORM\UniqueConstraint(name="i_lang", columns={"code"})})
 * @ORM\Entity
 */
class TrLang
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=11, nullable=false)
     */
    private $code = '';

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=60, nullable=false)
     */
    private $name = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="name_local", type="string", length=60, nullable=true)
     */
    private $nameLocal;

    /**
     * @var int
     *
     * @ORM\Column(name="backend_active", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $backendActive = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="beta_testing", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $betaTesting = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="item_order", type="integer", nullable=true)
     */
    private $itemOrder;

    /**
     * @var int|null
     *
     * @ORM\Column(name="rubicus_id", type="integer", nullable=true)
     */
    private $rubicusId;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_locked", type="boolean", nullable=false)
     */
    private $isLocked = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="export_lang", type="boolean", nullable=true)
     */
    private $exportLang;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNameLocal(): ?string
    {
        return $this->nameLocal;
    }

    public function setNameLocal(?string $nameLocal): self
    {
        $this->nameLocal = $nameLocal;

        return $this;
    }

    public function getBackendActive(): ?int
    {
        return $this->backendActive;
    }

    public function setBackendActive(int $backendActive): self
    {
        $this->backendActive = $backendActive;

        return $this;
    }

    public function getBetaTesting(): ?int
    {
        return $this->betaTesting;
    }

    public function setBetaTesting(int $betaTesting): self
    {
        $this->betaTesting = $betaTesting;

        return $this;
    }

    public function getItemOrder(): ?int
    {
        return $this->itemOrder;
    }

    public function setItemOrder(?int $itemOrder): self
    {
        $this->itemOrder = $itemOrder;

        return $this;
    }

    public function getRubicusId(): ?int
    {
        return $this->rubicusId;
    }

    public function setRubicusId(?int $rubicusId): self
    {
        $this->rubicusId = $rubicusId;

        return $this;
    }

    public function getIsLocked(): ?bool
    {
        return $this->isLocked;
    }

    public function setIsLocked(bool $isLocked): self
    {
        $this->isLocked = $isLocked;

        return $this;
    }

    public function getExportLang(): ?bool
    {
        return $this->exportLang;
    }

    public function setExportLang(?bool $exportLang): self
    {
        $this->exportLang = $exportLang;

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
