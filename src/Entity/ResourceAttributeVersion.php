<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ResourceAttributeVersion
 *
 * @ORM\Table(name="resource_attribute_version", indexes={@ORM\Index(name="resource_attribute_version_FI_1", columns={"resource_version_id"})})
 * @ORM\Entity
 */
class ResourceAttributeVersion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="attribute_name", type="string", length=255, nullable=false)
     */
    private $attributeName = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="attribute_value", type="text", length=65535, nullable=true)
     */
    private $attributeValue;

    /**
     * @var ResourceVersion
     *
     * @ORM\ManyToOne(targetEntity="ResourceVersion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="resource_version_id", referencedColumnName="id")
     * })
     */
    private $resourceVersion;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAttributeName(): ?string
    {
        return $this->attributeName;
    }

    public function setAttributeName(string $attributeName): self
    {
        $this->attributeName = $attributeName;

        return $this;
    }

    public function getAttributeValue(): ?string
    {
        return $this->attributeValue;
    }

    public function setAttributeValue(?string $attributeValue): self
    {
        $this->attributeValue = $attributeValue;

        return $this;
    }

    public function getResourceVersion(): ?ResourceVersion
    {
        return $this->resourceVersion;
    }

    public function setResourceVersion(?ResourceVersion $resourceVersion): self
    {
        $this->resourceVersion = $resourceVersion;

        return $this;
    }


}
