<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TrTranslationData
 *
 * @ORM\Table(name="tr_translation_data", uniqueConstraints={@ORM\UniqueConstraint(name="unique", columns={"translation_unit_id", "lang_id"})}, indexes={@ORM\Index(name="tr_translation_data_FI_2", columns={"lang_id"}), @ORM\Index(name="text", columns={"text"}), @ORM\Index(name="i_lang_public", columns={"status", "lang_id", "translation_unit_id"}), @ORM\Index(name="tr_translation_data_FI_1", columns={"translation_unit_id"}), @ORM\Index(name="prod_version", columns={"prod_version"})})
 * @ORM\Entity(repositoryClass="App\Repository\TrTranslationDataRepository")
 */
class TrTranslationData
{
    /**
     * Stavy kterych muze nabyvat preklad
     * //todo udělat na to novou entitu/valueObject
     */
    const UPDATED = 'updated'; // oznaceny pro kontrolu a znovuprelozeny
    const TRANSLATED = 'translated'; // je prelozeny

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="text", type="text", length=65535, nullable=true)
     */
    private $text;

    /**
     * @var int|null
     *
     * @ORM\Column(name="version", type="integer", nullable=true)
     */
    private $version;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prod_text", type="text", length=65535, nullable=true)
     */
    private $prodText;

    /**
     * @var int|null
     *
     * @ORM\Column(name="prod_version", type="integer", nullable=true)
     */
    private $prodVersion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @var TrLang
     *
     * @ORM\ManyToOne(targetEntity="TrLang")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="lang_id", referencedColumnName="id")
     * })
     */
    private $lang;

    /**
     * @var TrTranslationUnit
     *
     * @ORM\ManyToOne(targetEntity="TrTranslationUnit", inversedBy="translationData")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="translation_unit_id", referencedColumnName="id")
     * })
     */
    private $translationUnit;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="ResourceVersion", mappedBy="resource")
     */
    private $resourceVersion;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getVersion(): ?int
    {
        return $this->version;
    }

    public function setVersion(?int $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getProdText(): ?string
    {
        return $this->prodText;
    }

    public function setProdText(?string $prodText): self
    {
        $this->prodText = $prodText;

        return $this;
    }

    public function getProdVersion(): ?int
    {
        return $this->prodVersion;
    }

    public function setProdVersion(?int $prodVersion): self
    {
        $this->prodVersion = $prodVersion;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getLang(): ?TrLang
    {
        return $this->lang;
    }

    public function setLang(?TrLang $lang): self
    {
        $this->lang = $lang;

        return $this;
    }

    public function getTranslationUnit(): ?TrTranslationUnit
    {
        return $this->translationUnit;
    }

    public function setTranslationUnit(?TrTranslationUnit $translationUnit): self
    {
        $this->translationUnit = $translationUnit;

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResourceVersion(): \Doctrine\Common\Collections\Collection
    {
        return $this->resourceVersion;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $resourceVersion
     * @return TrTranslationData
     */
    public function setResourceVersion(\Doctrine\Common\Collections\Collection $resourceVersion): TrTranslationData
    {
        $this->resourceVersion = $resourceVersion;
        return $this;
    }




}
