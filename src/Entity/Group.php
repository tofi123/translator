<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Group
 *
 * @ORM\Table(name="`group`", uniqueConstraints={@ORM\UniqueConstraint(name="branch", columns={"branch", "name"})}, indexes={@ORM\Index(name="IDX_6DC044C5BB861B1F", columns={"branch"})})
 * @ORM\Entity
 */
class Group
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=true)
     */
    private $name;

    /**
     * @var int|null
     *
     * @ORM\Column(name="active", type="integer", nullable=true)
     */
    private $active = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="visible", type="integer", nullable=true, options={"default"="1"})
     */
    private $visible = '1';

    /**
     * @var Branch
     *
     * @ORM\ManyToOne(targetEntity="Branch")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="branch", referencedColumnName="id")
     * })
     */
    private $branch;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="TrTranslationUnit", mappedBy="group")
     */
    private $translationUnit;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->translationUnit = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(?int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getVisible(): ?int
    {
        return $this->visible;
    }

    public function setVisible(?int $visible): self
    {
        $this->visible = $visible;

        return $this;
    }

    public function getBranch(): ?Branch
    {
        return $this->branch;
    }

    public function setBranch(?Branch $branch): self
    {
        $this->branch = $branch;

        return $this;
    }

    /**
     * @return Collection|TrTranslationUnit[]
     */
    public function getTranslationUnit(): Collection
    {
        return $this->translationUnit;
    }

    public function addTranslationUnit(TrTranslationUnit $translationUnit): self
    {
        if (!$this->translationUnit->contains($translationUnit)) {
            $this->translationUnit[] = $translationUnit;
            $translationUnit->addGroup($this);
        }

        return $this;
    }

    public function removeTranslationUnit(TrTranslationUnit $translationUnit): self
    {
        if ($this->translationUnit->contains($translationUnit)) {
            $this->translationUnit->removeElement($translationUnit);
            $translationUnit->removeGroup($this);
        }

        return $this;
    }

    public function __toString()
    {
        return (string) $this->getId();
    }

}
