<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TrTranslationUnit
 *
 * @ORM\Table(name="tr_translation_unit", uniqueConstraints={@ORM\UniqueConstraint(name="proj_idx", columns={"project_id", "identifier"})}, indexes={@ORM\Index(name="i_project_id", columns={"project_id"}), @ORM\Index(name="tr_translation_unit_context", columns={"context_id"}), @ORM\Index(name="i_status_public_project", columns={"status", "is_public", "project_id"}), @ORM\Index(name="i_status_public", columns={"status", "is_public"}), @ORM\Index(name="i_identifier", columns={"identifier"}), @ORM\Index(name="tr_export_file", columns={"export_file_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\TrTranslationUnitRepository")
 */
class TrTranslationUnit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="string", length=255, nullable=false)
     */
    private $identifier = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="context_text", type="text", length=65535, nullable=true)
     */
    private $contextText;

    /**
     * @var string|null
     *
     * @ORM\Column(name="context_image", type="string", length=255, nullable=true)
     */
    private $contextImage;

    /**
     * @var string|null
     *
     * @ORM\Column(name="metadata", type="text", length=65535, nullable=true)
     */
    private $metadata;

    /**
     * @var int|null
     *
     * @ORM\Column(name="is_public", type="integer", nullable=true)
     */
    private $isPublic;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50, nullable=false, options={"default"="new","comment"="aktualni stav podle workflow"})
     */
    private $status = 'new';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="status_updated_at", type="datetime", nullable=true, options={"comment"="datum a cas posledni zmeny ve workflow"})
     */
    private $statusUpdatedAt;

    /**
     * @var int|null
     *
     * @ORM\Column(name="is_marked", type="integer", nullable=true)
     */
    private $isMarked;

    /**
     * @var TrExportFile
     *
     * @ORM\ManyToOne(targetEntity="TrExportFile")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="export_file2_id", referencedColumnName="id")
     * })
     */
    private $exportFile2;

    /**
     * @var int|null
     *
     * @ORM\Column(name="item_order", type="integer", nullable=true)
     */
    private $itemOrder = '0';

    /**
     * @var bool
     *
     * @ORM\Column(name="is_deprecated", type="boolean", nullable=false)
     */
    private $isDeprecated = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="tag", type="string", length=255, nullable=true)
     */
    private $tag;

    /**
     * @var TrExportFile
     *
     * @ORM\ManyToOne(targetEntity="TrExportFile")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="export_file_id", referencedColumnName="id")
     * })
     */
    private $exportFile;

    /**
     * @var TrProject
     *
     * @ORM\ManyToOne(targetEntity="TrProject")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;

    /**
     * @var TrTranslationUnitContext
     *
     * @ORM\ManyToOne(targetEntity="TrTranslationUnitContext")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="context_id", referencedColumnName="id")
     * })
     */
    private $context;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Group", inversedBy="translationUnit")
     * @ORM\JoinTable(name="unit_in_group",
     *   joinColumns={
     *     @ORM\JoinColumn(name="translation_unit_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     *   }
     * )
     */
    private $group;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="TrTranslationData", mappedBy="translationUnit")
     */
    private $translationData;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->group = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function getContextText(): ?string
    {
        return $this->contextText;
    }

    public function setContextText(?string $contextText): self
    {
        $this->contextText = $contextText;

        return $this;
    }

    public function getContextImage(): ?string
    {
        return $this->contextImage;
    }

    public function setContextImage(?string $contextImage): self
    {
        $this->contextImage = $contextImage;

        return $this;
    }

    public function getMetadata(): ?string
    {
        return $this->metadata;
    }

    public function setMetadata(?string $metadata): self
    {
        $this->metadata = $metadata;

        return $this;
    }

    public function getIsPublic(): ?int
    {
        return $this->isPublic;
    }

    public function setIsPublic(?int $isPublic): self
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getStatusUpdatedAt(): ?\DateTimeInterface
    {
        return $this->statusUpdatedAt;
    }

    public function setStatusUpdatedAt(?\DateTimeInterface $statusUpdatedAt): self
    {
        $this->statusUpdatedAt = $statusUpdatedAt;

        return $this;
    }

    public function getIsMarked(): ?int
    {
        return $this->isMarked;
    }

    public function setIsMarked(?int $isMarked): self
    {
        $this->isMarked = $isMarked;

        return $this;
    }

    public function getExportFile2(): ?TrExportFile
    {
        return $this->exportFile2;
    }

    public function setExportFile2(?TrExportFile $exportFile2): self
    {
        $this->exportFile2 = $exportFile2;

        return $this;
    }

    public function getItemOrder(): ?int
    {
        return $this->itemOrder;
    }

    public function setItemOrder(?int $itemOrder): self
    {
        $this->itemOrder = $itemOrder;

        return $this;
    }

    public function getIsDeprecated(): ?bool
    {
        return $this->isDeprecated;
    }

    public function setIsDeprecated(bool $isDeprecated): self
    {
        $this->isDeprecated = $isDeprecated;

        return $this;
    }

    public function getTag(): ?string
    {
        return $this->tag;
    }

    public function setTag(?string $tag): self
    {
        $this->tag = $tag;

        return $this;
    }

    public function getExportFile(): ?TrExportFile
    {
        return $this->exportFile;
    }

    public function setExportFile(?TrExportFile $exportFile): self
    {
        $this->exportFile = $exportFile;

        return $this;
    }

    public function getProject(): ?TrProject
    {
        return $this->project;
    }

    public function setProject(?TrProject $project): self
    {
        $this->project = $project;

        return $this;
    }

    public function getContext(): ?TrTranslationUnitContext
    {
        return $this->context;
    }

    public function setContext(?TrTranslationUnitContext $context): self
    {
        $this->context = $context;

        return $this;
    }

    /**
     * @return Collection|Group[]
     */
    public function getGroup(): Collection
    {
        return $this->group;
    }

    public function addGroup(Group $group): self
    {
        if (!$this->group->contains($group)) {
            $this->group[] = $group;
        }

        return $this;
    }

    public function removeGroup(Group $group): self
    {
        if ($this->group->contains($group)) {
            $this->group->removeElement($group);
        }

        return $this;
    }

    /**
     * @return Collection
     */
    public function getTranslationData(): Collection
    {
        return $this->translationData;
    }

    /**
     * @param Collection $translationData
     * @return TrTranslationUnit
     */
    public function setTranslationData(Collection $translationData): TrTranslationUnit
    {
        $this->translationData = $translationData;
        return $this;
    }




    public function __toString()
    {
        return (string) $this->getId();
    }
}
