<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TrExportFile
 *
 * @ORM\Table(name="tr_export_file", uniqueConstraints={@ORM\UniqueConstraint(name="proj_idx", columns={"identifier"})})
 * @ORM\Entity
 */
class TrExportFile
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="string", length=60, nullable=false)
     */
    private $identifier = '';

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=60, nullable=false)
     */
    private $name = '';

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=20, nullable=false)
     */
    private $type = '';

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255, nullable=false)
     */
    private $location = '';

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255, nullable=false)
     */
    private $filename = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="metadata", type="text", length=65535, nullable=true)
     */
    private $metadata;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="one_file", type="integer", nullable=false, options={"comment"="vsechny jazyky do jednoho souboru"})
     */
    private $oneFile = '0';

    /**
     * @var bool
     *
     * @ORM\Column(name="lockable", type="boolean", nullable=false)
     */
    private $lockable = '0';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function getMetadata(): ?string
    {
        return $this->metadata;
    }

    public function setMetadata(?string $metadata): self
    {
        $this->metadata = $metadata;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getOneFile(): ?int
    {
        return $this->oneFile;
    }

    public function setOneFile(int $oneFile): self
    {
        $this->oneFile = $oneFile;

        return $this;
    }

    public function getLockable(): ?bool
    {
        return $this->lockable;
    }

    public function setLockable(bool $lockable): self
    {
        $this->lockable = $lockable;

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

}
