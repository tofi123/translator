<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UnitMeta
 *
 * @ORM\Table(name="unit_meta", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQUE", columns={"translation_unit_id", "meta"})}, indexes={@ORM\Index(name="IDX_5B34F56F59B189FF", columns={"translation_unit_id"})})
 * @ORM\Entity
 */
class UnitMeta
{
    /**
     * @var string
     *
     * @ORM\Column(name="meta", type="string", length=4, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $meta;

    /**
     * @var TrTranslationUnit
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="TrTranslationUnit")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="translation_unit_id", referencedColumnName="id")
     * })
     */
    private $translationUnit;

    public function getMeta(): ?string
    {
        return $this->meta;
    }

    public function getTranslationUnit(): ?TrTranslationUnit
    {
        return $this->translationUnit;
    }

    public function setTranslationUnit(?TrTranslationUnit $translationUnit): self
    {
        $this->translationUnit = $translationUnit;

        return $this;
    }


}
