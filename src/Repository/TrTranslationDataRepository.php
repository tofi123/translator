<?php
namespace App\Repository;

use App\Entity\TrLang;
use App\Entity\TrTranslationData;
use App\Entity\TrTranslationUnit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class TrTranslationDataRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TrTranslationData::class);
    }

    public function getByLangUnit(TrLang $lang, TrTranslationUnit $translationUnit)
    {
        $qb = $this->createQueryBuilder('td')
            ->select('td', 'rv')
            ->join('td.resourceVersion', 'rv', 'WITH', "rv.resource = td.id AND rv.number = td.version")
            ->andWhere('td.translationUnit = :translationUnit')
            ->andWhere('td.lang = :lang')
            ->setParameter('translationUnit', $translationUnit->getId())
            ->setParameter('lang', $lang->getId())
            ->getQuery();

        $result = $qb->getSingleResult();

        return $result;
    }
}