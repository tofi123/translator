<?php
namespace App\Repository;

use App\Entity\TrLang;
use App\Entity\TrProject;
use App\Entity\TrTranslationUnit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class TrTranslationUnitRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TrTranslationUnit::class);
    }

    public function findByLangProjectQueryBuilder(TrLang $lang, TrProject $project, TrLang $langRef)
    {
        $qb = $this->createQueryBuilder('tu')
            ->select('tu', 'td1', 'td2')
            ->join('tu.translationData', 'td1', 'WITH', 'td1.translationUnit = tu.id AND td1.lang = :lang')
            ->join('tu.translationData', 'td2', 'WITH', 'td2.translationUnit = tu.id AND td2.lang = :langRef')
            ->andWhere('tu.project = :project')
            ->setParameter('project', $project->getId())
            ->setParameter('lang', $lang->getId())
            ->setParameter('langRef', $langRef->getId());

        return $qb;
    }

    public function findAllQueryBuilder()
    {
        return $this->createQueryBuilder('tu')->orderBy('tu.id', 'desc');
    }
}