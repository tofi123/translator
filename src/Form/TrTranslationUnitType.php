<?php

namespace App\Form;

use App\Entity\TrTranslationUnit;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TrTranslationUnitType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('identifier')
            ->add('contextText')
            ->add('contextImage')
            ->add('metadata')
            ->add('isPublic')
            ->add('status')
            ->add('statusUpdatedAt')
            ->add('isMarked')
            ->add('exportFile2')
            ->add('itemOrder')
            ->add('isDeprecated')
            ->add('tag')
            ->add('exportFile')
            ->add('project')
            ->add('context')
            ->add('group')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TrTranslationUnit::class,
        ]);
    }
}
