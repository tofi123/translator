<?php

namespace App\Form;

use App\Entity\SfGuardUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SfGuardUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('algorithm')
            ->add('salt')
            ->add('password')
            ->add('createdAt')
            ->add('lastLogin')
            ->add('isActive')
            ->add('isSuperAdmin')
            ->add('refLang')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SfGuardUser::class,
        ]);
    }
}
