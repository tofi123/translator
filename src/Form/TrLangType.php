<?php

namespace App\Form;

use App\Entity\TrLang;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TrLangType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code')
            ->add('name')
            ->add('nameLocal')
            ->add('backendActive')
            ->add('betaTesting')
            ->add('itemOrder')
            ->add('rubicusId')
            ->add('isLocked')
            ->add('exportLang')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TrLang::class,
        ]);
    }
}
