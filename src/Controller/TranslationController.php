<?php
/**
 * Created by PhpStorm.
 * User: tom
 * Date: 20.7.18
 * Time: 19:49
 */

namespace App\Controller;

use App\Entity\TrLang;
use App\Entity\TrProject;
use App\Entity\TrTranslationData;
use App\Entity\TrTranslationUnit;
use App\Repository\TrTranslationDataRepository;
use App\Repository\TrTranslationUnitRepository;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/translation")
 */
class TranslationController extends Controller
{
    /**
     * @Route("/list/lang/{lang}/project/{project}", name="translation_list", methods="GET")
     *
     * @param $request
     * @param $lang
     * @param $project
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function list(Request $request, $lang, $project)
    {
        $page = $request->query->get('page', 1);
        $lang = $this->getLang($lang);
        $langRef = $this->getRefLang();

        $project = $this->getDoctrine()
            ->getRepository(TrProject::class)
            ->find($project);

        /**
         * @var $translationUnitRepository TrTranslationUnitRepository
         */
        $translationUnitRepository = $this->getDoctrine()->getRepository(TrTranslationUnit::class);

        $translationUnitsQB = $translationUnitRepository->findByLangProjectQueryBuilder($lang, $project, $langRef);

        $adapter = new DoctrineORMAdapter($translationUnitsQB);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(20);
        $pagerfanta->setCurrentPage($page);

        return $this->render(
            'translation/list.html.twig',
            [
                'translationUnits' => $pagerfanta->getCurrentPageResults(),
                'lang' => $lang,
                'langRef' => $langRef,
                'project' => $project,
                'pager' => $pagerfanta,
            ]
        );
    }

    /**
     * @Route("/edit/lang/{lang}/tu/{translationUnit}", name="translation_detail", methods="GET")
     *
     * @param $lang
     * @param $translationUnit
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detail($lang, $translationUnit)
    {
        $lang = $this->getLang($lang);
        $langRef = $this->getRefLang();


        /**
         * @var $translationUnitRepository TrTranslationUnitRepository
         */
        $translationUnitRepository = $this->getDoctrine()->getRepository(TrTranslationUnit::class);

        $translationUnit = $translationUnitRepository->find($translationUnit);


        /**
         * @var $translationDataRepository TrTranslationDataRepository
         */
        $translationDataRepository = $this->getDoctrine()->getRepository(TrTranslationData::class);

        $translationData = $translationDataRepository->getByLangUnit($lang, $translationUnit);
        $translationDataRef = $translationDataRepository->getByLangUnit($langRef, $translationUnit);

        return $this->render(
            'translation/edit.html.twig',
            [
                'translationUnit' => $translationUnit,
                'translationData' => $translationData,
                'translationDataRef' => $translationDataRef,
                'lang' => $lang,
                'langRef' => $langRef
            ]
        );
    }


    private function getLang($code)
    {
        $langRepository = $this->getDoctrine()->getRepository(TrLang::class);
        return $langRepository->findOneBy(['code' => $code]);
    }

    private function getRefLang()
    {
        $langRepository = $this->getDoctrine()->getRepository(TrLang::class);
        return $langRepository->findOneBy(['code' => 'en']);
    }
}