<?php

namespace App\Controller;

use App\Entity\TrLang;
use App\Form\TrLangType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/lang")
 */
class TrLangController extends Controller
{
    /**
     * @Route("/", name="tr_lang_index", methods="GET")
     */
    public function index(): Response
    {
        $trLangs = $this->getDoctrine()
            ->getRepository(TrLang::class)
            ->findAll();

        return $this->render('tr_lang/index.html.twig', ['tr_langs' => $trLangs]);
    }

    /**
     * @Route("/new", name="tr_lang_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $trLang = new TrLang();
        $form = $this->createForm(TrLangType::class, $trLang);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($trLang);
            $em->flush();

            return $this->redirectToRoute('tr_lang_index');
        }

        return $this->render('tr_lang/new.html.twig', [
            'tr_lang' => $trLang,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tr_lang_show", methods="GET")
     */
    public function show(TrLang $trLang): Response
    {
        return $this->render('tr_lang/show.html.twig', ['tr_lang' => $trLang]);
    }

    /**
     * @Route("/{id}/edit", name="tr_lang_edit", methods="GET|POST")
     */
    public function edit(Request $request, TrLang $trLang): Response
    {
        $form = $this->createForm(TrLangType::class, $trLang);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tr_lang_edit', ['id' => $trLang->getId()]);
        }

        return $this->render('tr_lang/edit.html.twig', [
            'tr_lang' => $trLang,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tr_lang_delete", methods="DELETE")
     */
    public function delete(Request $request, TrLang $trLang): Response
    {
        if ($this->isCsrfTokenValid('delete'.$trLang->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($trLang);
            $em->flush();
        }

        return $this->redirectToRoute('tr_lang_index');
    }
}
