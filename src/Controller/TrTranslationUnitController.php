<?php

namespace App\Controller;

use App\Entity\TrTranslationUnit;
use App\Form\TrTranslationUnitType;
use App\Repository\TrTranslationUnitRepository;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/translation/unit")
 */
class TrTranslationUnitController extends Controller
{
    /**
     * @Route("/", name="tr_translation_unit_index", methods="GET")
     */
    public function index(Request $request): Response
    {
        $page = $request->query->get('page', 1);

        /**
         * @var TrTranslationUnitRepository $trTranslationUnitRepository
         */
        $trTranslationUnitRepository = $this->getDoctrine()
            ->getRepository(TrTranslationUnit::class);
        $translationUnitsQB = $trTranslationUnitRepository->findAllQueryBuilder();

        $adapter = new DoctrineORMAdapter($translationUnitsQB);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(20);
        $pagerfanta->setCurrentPage($page);

        return $this->render(
            'tr_translation_unit/index.html.twig',
            [
                'tr_translation_units' => $pagerfanta->getCurrentPageResults(),
                'pager' => $pagerfanta
            ]
        );
    }

    /**
     * @Route("/new", name="tr_translation_unit_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $trTranslationUnit = new TrTranslationUnit();
        $form = $this->createForm(TrTranslationUnitType::class, $trTranslationUnit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($trTranslationUnit);
            $em->flush();

            return $this->redirectToRoute('tr_translation_unit_index');
        }

        return $this->render('tr_translation_unit/new.html.twig', [
            'tr_translation_unit' => $trTranslationUnit,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tr_translation_unit_show", methods="GET")
     */
    public function show(TrTranslationUnit $trTranslationUnit): Response
    {
        return $this->render('tr_translation_unit/show.html.twig', ['tr_translation_unit' => $trTranslationUnit]);
    }

    /**
     * @Route("/{id}/edit", name="tr_translation_unit_edit", methods="GET|POST")
     */
    public function edit(Request $request, TrTranslationUnit $trTranslationUnit): Response
    {
        $form = $this->createForm(TrTranslationUnitType::class, $trTranslationUnit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tr_translation_unit_edit', ['id' => $trTranslationUnit->getId()]);
        }

        return $this->render('tr_translation_unit/edit.html.twig', [
            'tr_translation_unit' => $trTranslationUnit,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tr_translation_unit_delete", methods="DELETE")
     */
    public function delete(Request $request, TrTranslationUnit $trTranslationUnit): Response
    {
        if ($this->isCsrfTokenValid('delete'.$trTranslationUnit->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($trTranslationUnit);
            $em->flush();
        }

        return $this->redirectToRoute('tr_translation_unit_index');
    }
}
