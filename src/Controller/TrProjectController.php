<?php

namespace App\Controller;

use App\Entity\TrProject;
use App\Form\TrProjectType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/project")
 */
class TrProjectController extends Controller
{
    /**
     * @Route("/", name="tr_project_index", methods="GET")
     */
    public function index(): Response
    {
        $trProjects = $this->getDoctrine()
            ->getRepository(TrProject::class)
            ->findAll();

        return $this->render('tr_project/index.html.twig', ['tr_projects' => $trProjects]);
    }

    /**
     * @Route("/new", name="tr_project_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $trProject = new TrProject();
        $form = $this->createForm(TrProjectType::class, $trProject);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($trProject);
            $em->flush();

            return $this->redirectToRoute('tr_project_index');
        }

        return $this->render('tr_project/new.html.twig', [
            'tr_project' => $trProject,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tr_project_show", methods="GET")
     */
    public function show(TrProject $trProject): Response
    {
        return $this->render('tr_project/show.html.twig', ['tr_project' => $trProject]);
    }

    /**
     * @Route("/{id}/edit", name="tr_project_edit", methods="GET|POST")
     */
    public function edit(Request $request, TrProject $trProject): Response
    {
        $form = $this->createForm(TrProjectType::class, $trProject);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tr_project_edit', ['id' => $trProject->getId()]);
        }

        return $this->render('tr_project/edit.html.twig', [
            'tr_project' => $trProject,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tr_project_delete", methods="DELETE")
     */
    public function delete(Request $request, TrProject $trProject): Response
    {
        if ($this->isCsrfTokenValid('delete'.$trProject->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($trProject);
            $em->flush();
        }

        return $this->redirectToRoute('tr_project_index');
    }
}
