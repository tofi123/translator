<?php
namespace App\Controller;

use App\Entity\TrProject;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomePageController extends AbstractController
{
    /**
     * Homepage
     *
     * @Route("/", name="homepage")
     */
    public function index()
    {
        $projects = $this->getDoctrine()
            ->getRepository(TrProject::class)
            ->findAll();

        return $this->render('homepage/index.html.twig', ['projects' => $projects]);
    }
}