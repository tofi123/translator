<?php

namespace App\Controller;

use App\Entity\SfGuardUser;
use App\Form\SfGuardUserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user")
 */
class SfGuardUserController extends Controller
{
    /**
     * @Route("/", name="sf_guard_user_index", methods="GET")
     */
    public function index(): Response
    {
        $sfGuardUsers = $this->getDoctrine()
            ->getRepository(SfGuardUser::class)
            ->findAll();

        return $this->render('sf_guard_user/index.html.twig', ['sf_guard_users' => $sfGuardUsers]);
    }

    /**
     * @Route("/new", name="sf_guard_user_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $sfGuardUser = new SfGuardUser();
        $form = $this->createForm(SfGuardUserType::class, $sfGuardUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($sfGuardUser);
            $em->flush();

            return $this->redirectToRoute('sf_guard_user_index');
        }

        return $this->render('sf_guard_user/new.html.twig', [
            'sf_guard_user' => $sfGuardUser,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sf_guard_user_show", methods="GET")
     */
    public function show(SfGuardUser $sfGuardUser): Response
    {
        return $this->render('sf_guard_user/show.html.twig', ['sf_guard_user' => $sfGuardUser]);
    }

    /**
     * @Route("/{id}/edit", name="sf_guard_user_edit", methods="GET|POST")
     */
    public function edit(Request $request, SfGuardUser $sfGuardUser): Response
    {
        $form = $this->createForm(SfGuardUserType::class, $sfGuardUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sf_guard_user_edit', ['id' => $sfGuardUser->getId()]);
        }

        return $this->render('sf_guard_user/edit.html.twig', [
            'sf_guard_user' => $sfGuardUser,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sf_guard_user_delete", methods="DELETE")
     */
    public function delete(Request $request, SfGuardUser $sfGuardUser): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sfGuardUser->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($sfGuardUser);
            $em->flush();
        }

        return $this->redirectToRoute('sf_guard_user_index');
    }
}
