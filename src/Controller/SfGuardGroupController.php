<?php

namespace App\Controller;

use App\Entity\SfGuardGroup;
use App\Form\SfGuardGroupType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/group")
 */
class SfGuardGroupController extends Controller
{
    /**
     * @Route("/", name="sf_guard_group_index", methods="GET")
     */
    public function index(): Response
    {
        $sfGuardGroups = $this->getDoctrine()
            ->getRepository(SfGuardGroup::class)
            ->findAll();

        return $this->render('sf_guard_group/index.html.twig', ['sf_guard_groups' => $sfGuardGroups]);
    }

    /**
     * @Route("/new", name="sf_guard_group_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $sfGuardGroup = new SfGuardGroup();
        $form = $this->createForm(SfGuardGroupType::class, $sfGuardGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($sfGuardGroup);
            $em->flush();

            return $this->redirectToRoute('sf_guard_group_index');
        }

        return $this->render('sf_guard_group/new.html.twig', [
            'sf_guard_group' => $sfGuardGroup,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sf_guard_group_show", methods="GET")
     */
    public function show(SfGuardGroup $sfGuardGroup): Response
    {
        return $this->render('sf_guard_group/show.html.twig', ['sf_guard_group' => $sfGuardGroup]);
    }

    /**
     * @Route("/{id}/edit", name="sf_guard_group_edit", methods="GET|POST")
     */
    public function edit(Request $request, SfGuardGroup $sfGuardGroup): Response
    {
        $form = $this->createForm(SfGuardGroupType::class, $sfGuardGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sf_guard_group_edit', ['id' => $sfGuardGroup->getId()]);
        }

        return $this->render('sf_guard_group/edit.html.twig', [
            'sf_guard_group' => $sfGuardGroup,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sf_guard_group_delete", methods="DELETE")
     */
    public function delete(Request $request, SfGuardGroup $sfGuardGroup): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sfGuardGroup->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($sfGuardGroup);
            $em->flush();
        }

        return $this->redirectToRoute('sf_guard_group_index');
    }
}
