<?php

namespace App\Controller;

use App\Entity\SfGuardPermission;
use App\Form\SfGuardPermissionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/permission")
 */
class SfGuardPermissionController extends Controller
{
    /**
     * @Route("/", name="sf_guard_permission_index", methods="GET")
     */
    public function index(): Response
    {
        $sfGuardPermissions = $this->getDoctrine()
            ->getRepository(SfGuardPermission::class)
            ->findAll();

        return $this->render('sf_guard_permission/index.html.twig', ['sf_guard_permissions' => $sfGuardPermissions]);
    }

    /**
     * @Route("/new", name="sf_guard_permission_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $sfGuardPermission = new SfGuardPermission();
        $form = $this->createForm(SfGuardPermissionType::class, $sfGuardPermission);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($sfGuardPermission);
            $em->flush();

            return $this->redirectToRoute('sf_guard_permission_index');
        }

        return $this->render('sf_guard_permission/new.html.twig', [
            'sf_guard_permission' => $sfGuardPermission,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sf_guard_permission_show", methods="GET")
     */
    public function show(SfGuardPermission $sfGuardPermission): Response
    {
        return $this->render('sf_guard_permission/show.html.twig', ['sf_guard_permission' => $sfGuardPermission]);
    }

    /**
     * @Route("/{id}/edit", name="sf_guard_permission_edit", methods="GET|POST")
     */
    public function edit(Request $request, SfGuardPermission $sfGuardPermission): Response
    {
        $form = $this->createForm(SfGuardPermissionType::class, $sfGuardPermission);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sf_guard_permission_edit', ['id' => $sfGuardPermission->getId()]);
        }

        return $this->render('sf_guard_permission/edit.html.twig', [
            'sf_guard_permission' => $sfGuardPermission,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sf_guard_permission_delete", methods="DELETE")
     */
    public function delete(Request $request, SfGuardPermission $sfGuardPermission): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sfGuardPermission->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($sfGuardPermission);
            $em->flush();
        }

        return $this->redirectToRoute('sf_guard_permission_index');
    }
}
